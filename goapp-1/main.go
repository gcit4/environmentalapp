package main

import (
	"GoApp/goapp-1/routes"
)

func main() {
	routes.InitializeRoutes()
	// structToJason()
}

type User struct {
	Name string
	Id   int
}

// func structToJason() {
// 	user := User{Name: "Frank", Id: 1222}
// 	fmt.Println(user)
// 	b, err := json.Marshal(user)
// 	if err != nil {
// 		fmt.Printf("Error: %s", err)
// 		return
// 	}
// 	fmt.Println(string(b))
// }
