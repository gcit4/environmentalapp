package routes

import (
	"log"
	"net/http"

	"GoApp/goapp-1/controller"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	router := mux.NewRouter()
	router.HandleFunc("/home", controller.HomeHandler)
	// router.HandleFunc("/urlParameters/{myname}", controller.Parameterhander)
	// student handler
	router.HandleFunc("/student", controller.Addstudent).Methods("POST")
	router.HandleFunc("/student/{sid}", controller.GetStud).Methods("GET")
	router.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	router.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")
	router.HandleFunc("/students", controller.GetAllStuds)

	router.HandleFunc("/course", controller.Addcourse).Methods("POST")
	router.HandleFunc("/course/{cid}", controller.Getcour).Methods("GET")
	router.HandleFunc("/course/{cid}", controller.UpdateCourse).Methods("PUT")
	router.HandleFunc("/course/{cid}", controller.DeleteCourse).Methods("DELETE")
	router.HandleFunc("/courses", controller.GetAllCourses)

	router.HandleFunc("/enroll", controller.Enroll).Methods("POST")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")
	router.HandleFunc("/enroll/{sid}/{cid}", controller.DeleteEnroll).Methods("DELETE")
	router.HandleFunc("/enrolls", controller.GetEnrolls)

	router.HandleFunc("/signup", controller.Signup).Methods("POST")

	//login
	router.HandleFunc("/login", controller.Login).Methods("POST")

	fhandler := http.FileServer(http.Dir("./index"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8080...")
	err := http.ListenAndServe(":7070", router)
	if err != nil {
		return
	}
}
