package controller

import (
	"GoApp/goapp-1/model"
	httpResp "GoApp/goapp-1/utils/HttpResp"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("Hello world"))
	if err != nil {
		fmt.Println("error:", err)
	}

}

func Parameterhander(w http.ResponseWriter, r *http.Request) {
	para := mux.Vars(r) //return data oftype is map
	fmt.Println(para)
	name := para["myname"]
	fmt.Println(name)
	_, err := w.Write([]byte("My name is " + name))
	if err != nil {
		fmt.Println("error: ")
	}

}

func Signup(w http.ResponseWriter, r *http.Request) {

	var admin model.Admin

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&admin)

	if err != nil {
		httpResp.RespondWithJson(w, http.StatusBadRequest, "Invalid Json Typer")
		return
	}
	defer r.Body.Close()
	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "success"})
}

// login
func Login(w http.ResponseWriter, r *http.Request) {

	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   "my-value",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	var admin model.Login

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&admin)

	if err != nil {
		httpResp.RespondWithJson(w, http.StatusBadRequest, "Invalid json body")
		return
	}
	defer r.Body.Close()

	getErr := admin.Get()
	if getErr != nil {
		httpResp.ResponseWitherror(w, http.StatusUnauthorized, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "login success"})
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"message": "cookie deleted"})
}

func Addstudent(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "add student handler")
	var stud model.Student
	_ = stud

	decoder := json.NewDecoder(r.Body)
	// fmt.Println(decoder)

	if err := decoder.Decode(&stud); err != nil {
		// w.Write([]byte("Invalid json data"))
		response, _ := json.Marshal(map[string]string{"error": "Invalid Json Type"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	saveErr := stud.Create()
	fmt.Println(saveErr)
	if saveErr != nil {
		// w.Write([]byte("Database error"))
		response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}
	response, _ := json.Marshal(map[string]string{"status": "student added"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func GetStud(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	fmt.Println(sid)
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{Stdid: stdId}
	getErr := s.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.ResponseWitherror(w, http.StatusNotFound, "Student not found")
		default:
			httpResp.ResponseWitherror(w, http.StatusNotFound, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, s)
}

func getUserId(userIdParam string) (int64, error) {
	userId, userErr := strconv.ParseInt(userIdParam, 10, 64)
	if userErr != nil {
		return 0, userErr
	}
	return userId, nil
}

func UpdateStud(w http.ResponseWriter, r *http.Request) {
	old_sid := mux.Vars(r)["sid"]
	old_stdId, idErr := getUserId(old_sid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var stud model.Student
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&stud); err != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, "invalid jason body")
		return
	}
	defer r.Body.Close()

	err := stud.Update(old_stdId)
	if err != nil {
		httpResp.ResponseWitherror(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, stud)
}

func DeleteStud(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	stdId, idErr := getUserId(sid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	s := model.Student{Stdid: stdId}
	if err := s.Delete(); err != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllStuds(w http.ResponseWriter, r *http.Request) {
	students, getErr := model.GetAllStudents()
	if getErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, students)
}

func Addcourse(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprintf(w, "add student handler")
	var cour model.Course
	_ = cour

	decoder := json.NewDecoder(r.Body)
	// fmt.Println(decoder)

	if err := decoder.Decode(&cour); err != nil {
		// w.Write([]byte("Invalid json data"))
		response, _ := json.Marshal(map[string]string{"error": "Invalid Json Type"})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}

	defer r.Body.Close()

	saveErr := cour.Create()
	fmt.Println(saveErr)
	if saveErr != nil {
		// w.Write([]byte("Database error"))
		response, _ := json.Marshal(map[string]string{"error": saveErr.Error()})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		w.Write(response)
		return
	}
	//no error
	// w.Write([]byte("response success"))
	response, _ := json.Marshal(map[string]string{"status": "course added"})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(response)
}

func Getcour(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	fmt.Println(cid)
	courseid, idErr := getCourseId(cid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := model.Course{Courseid: courseid}
	getErr := c.Read()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.ResponseWitherror(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.ResponseWitherror(w, http.StatusNotFound, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, c)
}

func getCourseId(courseIdParam string) (string, error) {
	return courseIdParam, nil
}

func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"]
	old_courseId, idErr := getCourseId(old_cid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	var cour model.Course
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&cour); err != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, "invalid jason body")
		return
	}
	defer r.Body.Close()

	err := cour.Update(old_courseId)
	if err != nil {
		httpResp.ResponseWitherror(w, http.StatusInternalServerError, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, cour)
}

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]
	courseid, idErr := getCourseId(cid)
	if idErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, idErr.Error())
		return
	}
	c := model.Course{Courseid: courseid}
	if err := c.Delete(); err != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}

func GetAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := model.GetAllCourses()
	if getErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, courses)
}

func Enroll(w http.ResponseWriter, r *http.Request) {
	var e model.Enroll
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&e); err != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, "invalid json body")
		return
	}

	// e.Date_Enrolled = date.GetDate()
	e.Date_Enrolled = httpResp.GetDate()
	defer r.Body.Close()

	saveErr := e.EnrollStud()
	if saveErr != nil {
		if strings.Contains(saveErr.Error(), "duplicate key") {
			httpResp.ResponseWitherror(w, http.StatusForbidden, "Duplicate keys")
			return
		} else {
			httpResp.ResponseWitherror(w, http.StatusInternalServerError, saveErr.Error())
			fmt.Println("error is here", saveErr)
		}
	}
	httpResp.RespondWithJson(w, http.StatusCreated, map[string]string{"status": "enrolled"})

}

func GetEnroll(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	cid := mux.Vars(r)["cid"]

	stdid, _ := strconv.ParseInt(sid, 10, 64)

	e := model.Enroll{StdId: stdid, CourseID: cid}
	getErr := e.Get()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.ResponseWitherror(w, http.StatusNotFound, "No such Enrollments")
		default:
			httpResp.ResponseWitherror(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, e)
}

func GetEnrolls(w http.ResponseWriter, r *http.Request) {
	enrolls, getErr := model.GetAllEnrolls()
	if getErr != nil {
		httpResp.ResponseWitherror(w, http.StatusBadRequest, getErr.Error())
	}
	httpResp.RespondWithJson(w, http.StatusOK, enrolls)
}

func DeleteEnroll(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	cid := mux.Vars(r)["cid"]
	stdid, _ := strconv.ParseInt(sid, 10, 64)
	e := model.Enroll{StdId: stdid, CourseID: cid}

	if err := e.Delete(); err != nil {
		httpResp.RespondWithJson(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondWithJson(w, http.StatusOK, map[string]string{"status": "deleted"})
}
