package httpResp

import (
	"encoding/json"
	"net/http"
	"time"
)

func ResponseWitherror(w http.ResponseWriter, code int, message string) {
	RespondWithJson(w, code, map[string]string{"error": message})

}
func RespondWithJson(w http.ResponseWriter, code int, payload interface{}) {

	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}


	const apiDateLayout = "2006-01-02T15:04:05Z"

func GetDate() string {

	date := time.Now().UTC()          // get current date and time

	return date.Format(apiDateLayout) // convert to string


}


